"""
Используя ссылки в конце данного урока, ознакомьтесь с таким средством инкапсуляции как свойства.
Ознакомьтесь с декоратором property в Python. Создайте класс, описывающий температуру и позволяющий задавать и
получать температуру по шкале Цельсия и Фаренгейта, причём данные могут быть заданы в одной шкале, а получены в другой.
"""


class Temperature:
    def __init__(self, degrees, id_scale):
        self.__scale_map = {
            1: 'Celsius',
            2: 'Fahrenheit'
        }
        self.__degrees = degrees
        self.__scale = self.__get_scale(id_scale)

    def __str__(self):
        return f"{self.__degrees} degrees {self.__scale}"

    def __get_scale(self, id_scale):
        return self.__scale_map.get(id_scale, None)

    @property
    def scale(self):
        return self.__scale

    @property
    def degrees(self):
        return self.__degrees

    def get_temperature(self, id_scale):
        degrees_out = self.__degrees
        scale_out = self.__get_scale(id_scale)
        if self.__scale == 'Celsius' and scale_out == 'Fahrenheit':
            degrees_out = self.__degrees * 1.8 + 32
        elif self.__scale == 'Fahrenheit' and scale_out == 'Celsius':
            degrees_out = (self.__degrees - 32) / 1.8
        return degrees_out

    def set_temperature(self, degrees, id_scale):
        self.__scale = self.__get_scale(id_scale)
        self.__degrees = degrees


if __name__ == '__main__':
    temperature_obj = Temperature(100, 1)
    print(temperature_obj)
    print(temperature_obj.get_temperature(1))
    print(temperature_obj.get_temperature(2))
    print(temperature_obj.degrees, end=' ')
    print(temperature_obj.scale)
    temperature_obj.set_temperature(451, 2)
    print(temperature_obj)
    print(temperature_obj.get_temperature(1))
    print(temperature_obj.get_temperature(2))
    print(temperature_obj.degrees, end=' ')
    print(temperature_obj.scale)







