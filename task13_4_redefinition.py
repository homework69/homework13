"""
Опишите два класса Base и его наследника Child с методами method(),
который выводит на консоль фразы соответственно "Hello from Base" и "Hello from Child".
"""

# Щоб було цікавіше і IDE не просила статичних методів method, вирішив перевизначити метод __init__


class Base:
    def __init__(self):
        self.name_class = "Base"

    def method(self):
        print(f"Hello from {self.name_class}")


class Child(Base):
    def __init__(self):
        self.name_class = "Child"


if __name__ == '__main__':
    base_obj = Base()
    child_obj = Child()

    base_obj.method()
    child_obj.method()
