"""
Создайте класс, описывающий автомобиль. Какие атрибуты и методы должны быть полностью инкапсулированы?
Доступ к таким атрибутам и изменение данных реализуйте через специальные методы (get, set).
"""


class Car:
    def __init__(self, brand, model, vin_code):
        self.brand = brand
        self.model = model
        self.__vin_code = vin_code
        self.__power_map = {
            ("Fiat", "Linea"): 120,
            ("Fiat", "Tipo"): 95,
            ("Fiat", "500"): 75,
            ("Fiat", "500X"): 150,
            ("Fiat", "500L"): 110,
        }
        self.__power = self.__get_power(brand, model)

    def __get_power(self, brand, model):
        car_tuple = (brand, model)
        return self.__power_map.get(car_tuple, 0)

    @property
    def vin_code(self):
        return self.__vin_code

    @vin_code.setter
    def vin_code(self, value):
        self.__vin_code = value

    @property
    def power(self):
        if not self.__power:
            return "Undefined"
        return self.__power


if __name__ == '__main__':
    car1 = Car("Fiat", "500X", "ZFA324000000456")
    properties = (
        car1.brand,
        car1.model,
        car1.vin_code,
        car1.power
    )
    print(properties)

    car1.vin_code = "ZFA324000000123"
    print(car1.vin_code)
