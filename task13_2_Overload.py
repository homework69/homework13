"""
Создайте 2 класса – языка, например, английский и испанский. У обоих классов должен быть метод greeting().
Оба создают разные приветствия. Создайте два соответствующих объекта из двух классов выше и вызовите действия
этих двух объектов в одной функции (функция hello_friend).
"""

# Дозволив собі дещо змінити умову задачі і не створювати об'єктів класів,
# бо дуже хотілося протестувати роботу статичних методів


class English:
    @staticmethod
    def greeting():
        return "Hello my friend!"


class Spanish:
    @staticmethod
    def greeting():
        return "Hola mi amigo!"


def hello_friend(language):
    if language == 'English':
        return English.greeting()
    elif language == 'Spanish':
        return Spanish.greeting()
    else:
        print("Undefined language!")
        return ""


print(hello_friend('English'))
print(hello_friend('Spanish'))
print(hello_friend('French'))
